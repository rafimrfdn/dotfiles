# This is my Debian dotfiles

I include some `.config` of my Debian SID installation.

I prefer this config works well with [my DWM](https://github.com/rafimrfdn/dwm-debian) configuration.

## Font that I use

- JetBrains Mono
- SF Pro Display Medium

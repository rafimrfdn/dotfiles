" set copy paste antar aplikasi
vnoremap <C-c> "+y    # now use CTRL+c to copy the selected word or line in Visual Mode
" map <F12> "+P 		# for paste

" set Netrw as default file explorer instead of NerdTree
" default open with :Explore or :Sexplore or :Vexplore or :Sex
let g:netrw_banner = 0
